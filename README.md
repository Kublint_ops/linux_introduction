**What is Linux ?**

Linux is a free and open-source operating system that is based on the Unix operating system. It was created by Linus Torvalds in 1991 as a personal project, and has since grown into one of the most widely used operating systems in the world, powering everything from personal computers to servers and supercomputers.

![](./linux.jpg)

Linux is built on a modular architecture, which means that the core components of the operating system, such as the kernel and core utilities, can be customized and modified to suit different needs and use cases. This makes Linux highly adaptable and flexible, and allows it to be used in a wide variety of environments, from small embedded devices to large-scale data centers

**History of linux**

In 1983, Richard Stallman announced the GNU Project, an effort to create a free and open-source Unix-like operating system. The project included a number of key components, such as the GNU C Compiler (GCC) and the GNU General Public License (GPL), which would later become central to the development of Linux.

In 1991, Linus Torvalds, a computer science student at the University of Helsinki, began work on a new operating system kernel as a personal project. He based his work on the Minix operating system, a Unix-like system designed for educational purposes. Torvalds released the first version of his new kernel, which he named "Linux," in September of that year.

The early versions of Linux were very basic, but the project quickly attracted a community of developers and enthusiasts who contributed code and helped to improve the system. By the mid-1990s, Linux had become a popular alternative to proprietary Unix systems and was gaining a following in the academic and scientific communities.

In 1998, the Linux community adopted the Open Source Definition, which set out a set of principles and criteria for open-source software. This helped to establish Linux as a leading example of the open-source movement, and cemented its reputation as a flexible, adaptable, and community-driven operating system

**Types of Shell in linux**

1. Bash (Bourne-Again Shell): This is the default shell for most Linux distributions. It is a popular shell that is compatible with the original Bourne shell, but includes many additional features and enhancements. Bash is a powerful and versatile shell that can be used for a wide variety of tasks.

2. Korn shell (ksh): This shell was developed by David Korn at Bell Labs in the 1980s. It includes many features of the original Bourne shell, but also adds many advanced features such as command-line editing, history, and job control.

![shell](./shell.png)
 
3. C shell (csh): This shell was developed by Bill Joy at the University of California, Berkeley in the 1970s. It includes many advanced features, such as command-line editing and history, and is designed to be used by programmers and system administrators.

4. Z shell (zsh): This shell is similar to the Korn shell, but includes many additional features and enhancements. It includes features such as tab completion, spelling correction, and more advanced command-line editing.

5. Fish shell (fish): This is a modern shell that is designed to be user-friendly and intuitive. It includes many advanced features such as syntax highlighting, autosuggestions, and more

**Advantages of Linux as comparison to window**

**Cost:** Linux is free and open source, which means you can download, install, and use it without paying any licensing fees. In contrast, Windows operating systems can be expensive, especially for business or enterprise use.

**Stability:** Linux is known for being very stable and reliable, with fewer crashes, errors, and viruses compared to Windows. This is because Linux is built to be more secure and resilient to attacks.

**Customization:** Linux allows users to customize their operating system to suit their needs and preferences. Users can choose from a wide variety of desktop environments and software applications, and can even modify the source code of the operating system itself.

**Compatibility:** Linux can run on a wide variety of hardware platforms, from older computers to newer devices, without requiring extensive hardware upgrades. This makes it a popular choice for embedded systems, servers, and other specialized applications.

**Security:** Linux is known for being more secure than Windows, partly because it is less targeted by hackers and malware. Linux users also benefit from regular security updates and patches, as well as the ability to easily configure firewalls and other security settings


**Directory Structute in Linux**

The directory structure in Linux is organized as a hierarchical tree-like structure, starting from the root directory (/) and branching out into subdirectories. Here is a brief overview of some of the most commonly used directories in Linux:

**/:** The root directory is the top-level directory in the Linux file system.

**/bin:** This directory contains essential executable files that are required for the system to run, such as the ls and cp commands.

**/boot:** This directory contains the files necessary to boot the system, including the kernel and boot loader.

**/dev:** This directory contains device files that represent hardware devices connected to the system, such as hard drives, USB devices, and printers.

![directorystructure](./directory.png)

**/etc:** This directory contains configuration files for the system and applications installed on the system.

**/home:** This directory contains the home directories of regular users on the system.

**/lib:** This directory contains shared library files that are used by the system and applications.

**/mnt:** This directory is used to mount external file systems, such as USB drives or network file systems.

**/opt:** This directory is used to install third-party applications on the system.

**/proc:** This directory contains information about system resources and processes in a virtual file system.

**/sbin:** This directory contains system-level executables that are used for system administration purposes, such as the shutdown command.

**/tmp:** This directory is used for temporary files that are created by the system and applications.

**/usr:** This directory contains user-related programs and data, such as user applications and documentation.

**/var:** This directory contains variable files, such as log files and spool directories.

Note -- Linux Commands --- https://gitlab.com/DevOpsTouch/linux_commands/-/blob/main/README.md

Author:[ Somay Mangla](https://www.linkedin.com/in/er-somay-mangla/) and [DevOps Touch](https://www.linkedin.com/company/devopstouch/)


#devops #linux #devopstouch #somaymangla #community #docker #cicd #ansible #aws
